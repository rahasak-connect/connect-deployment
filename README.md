# Connect

Deployment of connect app services

### Services

Seven main servies now

```
1. elassandra
2. redis
3. aplos
4. gateway
5. notifier
6. trace-admin-web
7. trace-bank-web
8. trace-customer-web
9. groop
10. kibana
11. nginx
```

### configuration

change `host.docker.local` field in `.env` file to local machines ip. Also 
its possible to add a host entry to `/etc/hosts` file by overriding
`host.docker.local` with local machines ip. following is an example of
/etc/hosts file

```
10.4.1.104    host.docker.local
```

give write permission to `/private/var/services/connect/elassandra` directory 
in the server. following is the way to give the permission

```
sudo mkdir /private/var/services/connect/elassandra
sudo chmo -R 777 /private
```

### Deploy services

Start services in following order

```
docker-compose up -d elassandra
docker-compose up -d redis
docker-compose up -d aplos
docker-compose up -d gateway
docker-compose up -d notifier
docker-compose up -d trace-admin-web
docker-compose up -d trace-bank-web
docker-compose up -d trace-customer-web
docker-compose up -d nginx // optional to deploy
docker-compose up -d groop // optionsal to deploy
docker-compose up -d kibana // optional to deploy
```

### Connect mobile apps

gateway service will start a REST api on `7654` port. For an example if your
machines ip is `10.4.1.104` the apis can be access via `10.4.1.104:7654/api/<apiname>`.
Add this config to mobile app and connect to the services. in mbsl production/test 
envirounment use `10.0.8.94` as the api host
